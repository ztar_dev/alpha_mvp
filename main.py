from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
import json
import os

UPLOAD_FOLDER = 'upload'
ALLOWED_EXTENSIONS = set(['wav'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

if not os.path.isdir('upload'):
	try:
		os.mkdir('upload')
	except OSError:
		# Handle the case where the directory could not be created.
		print("unable to create file")

if not os.path.isfile('data.txt'):
	f = open("data.txt","w+")
	f.close()

def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/")
def index():
	return app.send_static_file("index.html")

@app.route("/createAccount", methods=['GET','POST'])
def createAccount():
	if request.method == 'POST':
		fName = request.form['firstName']
		lName = request.form['lastName']
		
		if not fName and not lName:
			return app.send_static_file("index.html")
		else:
			return app.send_static_file("account/access.html")
	else:
		return app.send_static_file("index.html")

@app.route("/welcome", methods=['GET','POST'])
def welcome():
	if request.method == 'POST':
		fName = request.form['firstName']
		lName = request.form['lastName']
		
		if not fName and not lName:
			return app.send_static_file("index.html")
		
		key = fName + lName
		newUserInfo ={"FirstName":request.form['firstName'], "LastName":request.form['lastName'], "voices":[]}

		if os.stat('data.txt').st_size == 0:
			data = {}
		else:
			with open('data.txt') as json_file:
				data = json.load(json_file)

		# check if the post request has the file part
		if 'file' not in request.files:
			print('File not in request.files!!!')
			return app.send_static_file("account/denied.html")
		file = request.files['file']
		if file.filename == '':
			print('Empty file name!!!')
			return app.send_static_file("account/denied.html")
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			print(filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
			if key in data:
				if len(data[key]["voices"]) < 2:
					data[key]["voices"].append(filename)
				else:
					return app.send_static_file("account/denied.html")

			else:
				newUserInfo["voices"].append(filename)
				data[key] = newUserInfo

			with open('data.txt', 'w') as f:
				json.dump(data, f, ensure_ascii=False)

			return app.send_static_file("account/welcome.html")

	else:
			return app.send_static_file("account/welcome.html")